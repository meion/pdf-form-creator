import { PxfObject } from "tls";

export enum PaperTypes {
    A4,
    A3
};

export enum ValidUnits {
    mm,
    cm
}

export enum ValidMonitorUnits {
    px
};
export interface PaperSize {
    width: number;
    height: number;
    unit: ValidUnits;
}

export interface MonitorSize {
     width: number;
     height: number;
     unit: ValidMonitorUnits;
}

export interface Point {
    x: number;
    y: number;
}
export interface Area {
    topLeftPoint: Point;
    topRightPoint: Point;
    bottomLeftPoint: Point;
    bottomRightPoint: Point;
}

export enum FontWeight {
    Light,
    Ligher,
    Normal,
    Bold,
    Bolder
}

export interface PaintOptions {
    fontSize?: number,
    font?: string,
    fontWeight?: FontWeight,

    italic?: boolean,
    underline?: boolean,
    strikethrough?: boolean,
    opaque?: boolean
}




abstract class BasePaper {
    abstract type: PaperTypes;
    abstract size: PaperSize;
    abstract virtualSize: MonitorSize;
    
    protected getReflectionArea(area: Area){
        const widthScale = this.getScale(this.size.width, this.virtualSize.width);
        const heightScale = this.getScale(this.size.height, this.virtualSize.height);

        const realArea : Area = {
            bottomLeftPoint: {
                x: area.bottomLeftPoint.x * widthScale,
                y: area.bottomLeftPoint.y * heightScale,  
            },
            bottomRightPoint: {
                x: area.bottomRightPoint.x * widthScale,
                y: area.bottomRightPoint.y * heightScale,  
            },
            topLeftPoint: {
                x: area.topLeftPoint.x * widthScale,
                y: area.topLeftPoint.y * heightScale,  
            },
            topRightPoint: {
                x: area.topRightPoint.x * widthScale,
                y: area.topRightPoint.y * heightScale,  
            },
        }
        return realArea; 
    }
    private getScale(real: number, virtual: number) {
        return real/virtual;
    }
}

abstract class Painter extends BasePaper {
    protected getMaxFontSize(area:Area) {
        const lowY = area.bottomLeftPoint.y;
        const highY = area.topLeftPoint.y;
        return Math.abs(lowY - highY);
    }
    protected getMaxContentLength(area:Area) {
        const lowX = area.bottomLeftPoint.x;
        const lowY = area.bottomRightPoint.x;
        return Math.abs(lowX - lowY);
    }
    private getContentLengthFromText(text: string) {
        return 0;
    }
    private validatePaintOptions(text: string, area: Area, options: PaintOptions) {
        if(options.fontSize && this.getMaxFontSize(area) > options.fontSize){
            return false;
        } else if(this.getContentLengthFromText(text) > this.getMaxContentLength(area)) {
            return false;
        }
        return true;
    }
    addText(text: string, area: Area, options?: PaintOptions) {
    }
}

export abstract class Paper extends Painter {
    abstract getAspectRatio(): number;
}

export const MAX_WIDTH = 600;

export default class A4 extends Paper {
    virtualSize: MonitorSize;
    type: PaperTypes;
    size: PaperSize;
    constructor(virtualSize?: MonitorSize){
        super();
        this.type = PaperTypes.A4;
        this.size = {
            height: 297,
            width: 210,
            unit: ValidUnits.mm
        };
        this.virtualSize = virtualSize || this.getDefaultSize();
        const virtualArea = {
            bottomLeftPoint: {
                x: 100,
                y: 100
            },
            bottomRightPoint:  {
                x: 200,
                y: 100
            },
            topLeftPoint:  {
                x: 100,
                y: 200
            },
            topRightPoint: {
                x: 200,
                y: 200
            },
        };
        let realArea = this.getReflectionArea(virtualArea);
        console.log(virtualArea);
        console.log(realArea);
    }
    getDefaultSize() : MonitorSize{
        const aspectRatio = this.getAspectRatio();
        let width = 1;
        let height = 1;
        for(let i = 0; width < MAX_WIDTH ; i++) {
            height += 1;
            width += aspectRatio;
        };
        return {
            height: Math.floor(height),
            width: Math.floor(width),
            unit: ValidMonitorUnits.px
        };
    }
    getAspectRatio(): number {
        return this.size.width / this.size.height;
    }
    
}

let paper = new A4();
console.log(paper);